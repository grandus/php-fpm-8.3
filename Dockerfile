ARG PHP_VERSION='8.3.2'
ARG ALPINE_VERSION='3.18'
FROM registry.gitlab.com/grandus/php-8.3-fpm-alpine-openssl-gost
WORKDIR /var/www
EXPOSE 9000
RUN apk --update --no-cache add \
                            git \
                            tzdata \
                            msmtp \
  && set -ex \
  && apk --no-cache add libxml2-dev \
                        libpng \
                        libpng-dev \
                        libjpeg \
                        libwebp-dev \
                        libjpeg-turbo-dev \
                        libzip-dev \
                        freetype-dev \
                        yaml \
                        yaml-dev \
                        poppler \
                        poppler-utils \
                        libldap \
                        linux-headers \
                        imagemagick \
                        imagemagick-dev \
                        postgresql-dev \
                        icu-dev \
  && apk add catdoc --repository=https://dl-cdn.alpinelinux.org/alpine/edge/testing/ \
  # Build dependancy for ldap \
  && apk add --update --no-cache --virtual .docker-php-ldap-dependencies \
                                           openldap-dev \
  && mkdir -p /usr/src/php/ext/memcache /usr/src/php/ext/xdebug /usr/src/php/ext/yaml /usr/src/php/ext/imagick \
  && curl -fsSL https://pecl.php.net/get/memcache --ipv4 | tar xvz -C "/usr/src/php/ext/memcache" --strip 1 \
  && curl -fsSL https://pecl.php.net/get/xdebug --ipv4 | tar xvz -C "/usr/src/php/ext/xdebug" --strip 1 \
  && curl -fsSL https://pecl.php.net/get/yaml --ipv4 | tar xvz -C "/usr/src/php/ext/yaml" --strip 1 \
  && curl -fsSL https://pecl.php.net/get/imagick --ipv4 | tar xvz -C "/usr/src/php/ext/imagick" --strip 1 \
  && docker-php-ext-configure gd --with-jpeg --with-freetype --with-jpeg --with-webp \
  && docker-php-ext-configure ldap \
  && docker-php-ext-configure imagick \
  && docker-php-ext-install mysqli \
                            pdo_pgsql \
                            soap \
                            sockets \
                            gd \
                            zip \
                            opcache \
                            memcache \
                            xdebug \
                            yaml \
                            ldap \
                            imagick \
                            intl \
  && docker-php-ext-enable  memcache \
                            yaml \
                            imagick \
  # mhsendmail for MailHog (https://github.com/mailhog/mhsendmail)
  && curl -LkSso /usr/bin/mhsendmail 'https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64' \
  && chmod +x /usr/bin/mhsendmail \
  && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer \
  && chown -R www-data:www-data /var/www \
  && apk del  .docker-php-ldap-dependencies \
              libxml2-dev \
              libpng-dev \
              libjpeg-turbo-dev \
              pkgconfig \
              linux-headers \
  && rm -f /sbin/apk \
  && rm -rf /etc/apk \
  && rm -rf /lib/apk \
  && rm -rf /usr/share/apk \
  && rm -rf /var/lib/apk \
  && rm -rf /usr/src

COPY docker-entrypoint.sh /docker-entrypoint.sh
COPY ./bitrix.ini /usr/local/etc/php/conf.d/common/bitrix.ini
ENTRYPOINT ["/docker-entrypoint.sh"]
CMD ["php-fpm"]
